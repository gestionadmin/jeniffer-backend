'use strict'

let express = require('express');
let ResrvationCtrlr = require('../controllers/reservation');
let api = express.Router();

api.post('', ResrvationCtrlr.saveReservation);
api.post('/printer', ResrvationCtrlr.printerReservation);
api.get('', ResrvationCtrlr.getAllReservation);
api.get('/print/:pdfFile', ResrvationCtrlr.getPdfReservation);
api.post('/filter', ResrvationCtrlr.getFilters);
api.put('/update/:id', ResrvationCtrlr.updateReservation);

module.exports = api;