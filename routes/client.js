'use strict'

let express = require('express');
let ClientCtrlr = require('../controllers/client');
let api = express.Router();

api.post('', ClientCtrlr.newClient);
api.get('', ClientCtrlr.getClients);
api.post('/filter', ClientCtrlr.getFilter);
api.put('/update/:id', ClientCtrlr.updateClient);

module.exports = api;
