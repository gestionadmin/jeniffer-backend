'use strict'

let express = require('express');
let Ubication = require('../controllers/ubication');
let api = express.Router();

api.get('/departaments', Ubication.getDepartaments);
api.get('/localities', Ubication.getLocalities);
api.get('/localities/:locality?', Ubication.getLocalitiesFilters);

module.exports = api;