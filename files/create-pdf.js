'use strict';

const fs = require('fs');
const PDF = require('pdfkit');

function printerReservationDress(reservation){
  let doc = new PDF();
  doc.pipe(fs.createWriteStream(`files/pdf/reservation-${reservation.numReservation}.pdf`));
  doc.roundedRect(25, 25, 565, 100,10)
    .stroke();
  doc.font('Times-Roman')
    .fontSize(20)
    .text(`Numero de resrvacion : ${reservation.numReservation}`,
      40,40)
    .moveDown(0.5);
  doc.end();
}

module.exports = {
  printerReservationDress
}