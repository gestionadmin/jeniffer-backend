'use strict'

var MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";

MongoClient.connect(url, function (err, dbres) {
  if (err) throw err;
  insertCounters(dbres);
});

function insertCounters(dbres) {
  let db = dbres.db('Jeniffer');
  db.collection('counters').drop();
  let counters = [
    {_id: 'reservation', seq : 0 },
    {_id: 'sale', seq : 0 },
    {_id: 'spending', seq : 0 },
    {_id: 'reservationMen', seq : 0 }
  ]
  db.collection('counters').insertMany(counters, (err, res) => {
    if (err) throw err;
    console.log("Number of documents inserted: " + res.insertedCount);
    dbres.close();
  });
}