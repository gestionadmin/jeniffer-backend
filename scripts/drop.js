'use strict'
var MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";

MongoClient.connect(url, function (err, dbres) {
  let db = dbres.db('Jeniffer');
  if (err) throw err;
  db.collection("departaments").drop();
  db.collection("clients").drop();
  db.collection("reservations").drop();
  let departamets = [
    {name: 'Cochinoca'},
    {name: 'Doctor Manuel Belgrano'},
    {name: 'El Carmen'},
    {name: 'Humahuaca'},
    {name: 'Ledesma'},
    {name: 'Palpalá'},
    {name: 'Rinconada'},
    {name: 'San Antonio'},
    {name: 'San Pedro'},
    {name: 'Santa Bárbara'},
    {name: 'Santa Catalina'},
    {name: 'Susques'},
    {name: 'Tilcara'},
    {name: 'Tumbaya'},
    {name: 'Valle Grande'},
    {name: 'Yavi'}
  ];
  let localities = [
    {name: 'Abdón Castro Tolay', departament: 'Cochinoca'},
    {name: 'Abralaite', departament: 'Cochinoca'},
    {name: 'Abra Pampa', departament: 'Cochinoca'},
    {name: 'Agua de Castilla', departament: 'Cochinoca'},
    {name: 'Casabindo', departament: 'Cochinoca'},
    {name: 'Cochinoca', departament: 'Cochinoca'},
    {name: 'La Redonda', departament: 'Cochinoca'},
    {name: 'Puesto del Marquéz', departament: 'Cochinoca'},
    {name: 'Quebraleña', departament: 'Cochinoca'},
    {name: 'Quera', departament: 'Cochinoca'},
    {name: 'Rinconadillas', departament: 'Cochinoca'},
    {name: 'San Francisco de Alfarcito', departament: 'Cochinoca'},
    {name: 'Santa Ana de la Puna', departament: 'Cochinoca'},
    {name: 'Santuario de Tres Pozos', departament: 'Cochinoca'},
    {name: 'Tambillos', departament: 'Cochinoca'},
    {name: 'Tusaquillas', departament: 'Cochinoca'},
    {name: 'Guerrero', departament: 'Doctor Manuel Belgrano'},
    {name: 'La Almona', departament: 'Doctor Manuel Belgrano'},
    {name: 'León', departament: 'Doctor Manuel Belgrano'},
    {name: 'Lozano ', departament: 'Doctor Manuel Belgrano'},
    {name: 'Ocloyas', departament: 'Doctor Manuel Belgrano'},
    {name: 'San Salvador de Jujuy', departament: 'Doctor Manuel Belgrano'},
    {name: 'Tesorero', departament: 'Doctor Manuel Belgrano'},
    {name: 'Yala', departament: 'Doctor Manuel Belgrano'}
  ]
  db.collection("departaments").insertMany(departamets, (err, res) => {
    if (err) throw err;
    console.log("Number of documents inserted: " + res.insertedCount);
    db.collection("localities").insertMany(localities, (err, res) => {
      if (err) throw err;
      console.log("Number of documents inserted: " + res.insertedCount);
      dbres.close();
    });
  });

});

