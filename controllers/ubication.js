'use strict'

let Departament = require('../models/departament');
let Locality = require('../models/locality');

function getDepartaments(req, res) {
  Departament.find().sort('name').exec((err, departaments) => {
    if (err) {
      res.status(500).send({msg: 'Error al obtener los Departamentos'});
    } else if (!departaments) {
      res.status(404).send({msg: 'No se pudo obtener los Departamentos'});
    } else {
      res.status(200).send({departaments});
    }
  });
}

function getLocalitiesFilters(req, res) {
  let locality = req.params.locality
  Locality.find({departament : locality},(err, localities) => {
    if (err) {
      res.status(500).send({msg: 'Error al obtener los Departamentos'});
    } else if (!localities) {
      res.status(404).send({msg: 'No se pudo obtener los Departamentos'});
    } else {
      res.status(200).send({localities});
    }
  });
}

function getLocalities(req, res) {
  Locality.find().sort('name').exec((err, localities) => {
    if (err) {
      res.status(500).send({msg: 'Error al obtener los Departamentos'});
    } else if (!localities) {
      res.status(404).send({msg: 'No se pudo obtener los Departamentos'});
    } else {
      res.status(200).send({localities});
    }
  });
}
module.exports = {
  getDepartaments,
  getLocalities,
  getLocalitiesFilters
}