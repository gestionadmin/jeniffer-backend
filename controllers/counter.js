'use strict'

let CounterModel = require('../models/counter');

function getNextSequence(name, callback) {
  console.log(name)
  CounterModel.findByIdAndUpdate( name,{ $inc: { seq: 1 } }, function (err,counter) {
    if(err){
      return err;
    }else {
      console.log(counter);
      return callback(counter.seq +1 );
    }
  })
}

module.exports = {
  getNextSequence
}