'use strict'
let mongoosePaginate = require('mongoose-pagination');
let Client = require('../models/client');

// funcion para agregar Cliente
function newClient(req, res) {
  let client = new Client();
  let params = req.body;
  client.name = params.name;
  client.lastName = params.lastName;
  client.document = params.document;
  client.phone = params.phone;
  client.line = params.line;
  client.mail = params.mail;
  client.wpp = params.wpp;
  client.street = params.street;
  client.number = params.number;
  client.district = params.district;
  client.locality = params.locality;
  client.state = params.state;

  client.save((err, clientSave) => {
    if (err) {
      res.status(500).send({msg: 'Error al guardar cliente'});
    } else if (!clientSave) {
      res.status(404).send({msg: 'No se pudo guardar el cliente'});
    } else {
      res.status(202).send({client: clientSave});
    }
  });
}

// function para obtener los clienteSave
function getClients(req, res) {

  Client.find().sort('name').exec((err, clients) => {
    if (err) {
      res.status(500).send({msg: 'Error al obtener los clientes'});
    } else if (!clients) {
      res.status(404).send({msg: 'No se pudo obtener los clientes'});
    } else {
      res.status(200).send({client: clients});
    }
  });
}

function getFilter(req, res) {
  let params = req.body;
  Client.find()
    .where((params.name != null ? {name: params.name} : {}))
    .where((params.lasName != null ? {lastName: params.lasName} : {}))
    .where((params.document != null ? {document: params.document} : {}))
    .where((params.phone != null ? {phone: params.phone} : {}))
    .where((params.mail != null ? {mail: params.mail} : {}))
    .where((params.locality != null ? {locality: params.locality} : {}))
    .exec((err,clients) => {
      if(err){
        res.status(500).send({msg:'Error al obtener al cliente'});
      }else if (!clients){
        res.status(404).send({msg:'No se pudo obtener al cliente'});
      }else{
        res.status(200).send({clients});
      }
    });
}

function updateClient(req, res){
  let id = req.params.id;
  let params = req.body;

  Client.findByIdAndUpdate(id , params, (err, client) =>{
    if(err){
      res.status(500).send({msg:'Error al actulizar al cliente'});
    }else if (!client){
      res.status(404).send({msg:'No se pudo actulizar el cliente'});
    }else{
      res.status(202).send({client});
    }
  });

}

module.exports = {
  newClient,
  getClients,
  getFilter,
  updateClient
}


