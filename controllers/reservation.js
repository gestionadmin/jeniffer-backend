'use strict';

let Reservation = require('../models/reservation');
let Counter = require('./counter');
let printer = require('../files/create-pdf');
const fs = require('fs');
const path = require('path');

function saveReservation(req, res) {
  let reservation = new Reservation();
  let param = req.body;

  Counter.getNextSequence('reservation', function (data) {
    reservation.numReservation = data;
    reservation.fhReservation = param.fhReservation;
    reservation.fhPresentation = param.fhPresentation;
    reservation.typeEvent = param.typeEvent;
    reservation.client = param.client;
    reservation.espalda = param.espalda;
    reservation.hombro = param.hombro;
    reservation.talleEspalda = param.talleEspalda;
    reservation.talleDelantero = param.talleDelantero;
    reservation.alturaBusto = param.alturaBusto;
    reservation.contornoBusto = param.contornoBusto;
    reservation.costado = param.costado;
    reservation.sisa = param.sisa;
    reservation.sisa_t = param.sisa_t;
    reservation.largoManga = param.largoManga;
    reservation.anchoManga = param.anchoManga;
    reservation.punio = param.punio;
    reservation.largoFalda = param.largoFalda;
    reservation.contornoCintura = param.contornoCintura;
    reservation.contornoCadera = param.contornoCadera;
    reservation.total = param.total;
    reservation.description = param.description;
    reservation.bordado = param.bordado;
    reservation.senia = param.senia;
    reservation.image = param.image;
    reservation.state = param.state;

    reservation.save((err, reservationSave) => {
      if (err) {
        res.status(500).send({msg: 'Error al guardar el Encargo', error: err});
      } else if (!reservationSave) {
        res.status(404).send({msg: 'No se pudo guardar el encargo'});
      } else {
        res.status(202).send({reservation: reservationSave});
      }
    });
  });
}

function getAllReservation(req, res) {
  let find = Reservation.find().sort('fhReservation');

  find.populate({path: 'client'}).exec((err, reservations) => {
    if (err) {
      console.log(err);
      res.status(500).send({msg: 'Error al guardar el Encargo'});
    } else if (!reservations) {
      res.status(404).send({msg: 'No se pudo guardar el encargo'});
    } else {
      res.status(200).send({reservations});
    }
  });
}

function getFilters(req, res) {
  let params = req.body;
  console.log(params);
  Reservation.find()
    .where((params.numReservation != null ? {numReservation: params.numReservation} : {}))
    .where((params.fhReservation != null ? {fhReservation: params.fhReservation} : {}))
    .where((params.fhPresentation != null ? {fhPresentation: params.fhPresentation} : {}))
    .where((params.typeEvent != null ? {typeEvent: params.typeEvent} : {}))
    .where((params.state != null ? {state: params.state} : {}))
    .exec((err, reservations) => {
      if (err) {
        console.log(err);
        res.status(500).send({msg: 'Error al obtener el Encargo'});
      } else if (!reservations) {
        res.status(404).send({msg: 'No se pudo pbtener el encargo'});
      } else {
        res.status(200).send({reservations});
      }
    });

}

function updateReservation(req, res) {
  let id = req.params.id;
  let param = req.body;

  Reservation.findByIdAndUpdate(id, param, (err, reservationUpdate) => {
    if (err) {
      res.status(500).send({msg: 'Error al actualizar la reservacion'});
    } else if (!reservationUpdate) {
      res.status(404).send({msg: 'No se pudo actualizar la reservacion'});
    } else {
      res.status(202).send({reservation: reservationUpdate});
    }
  });
}

function printerReservation(req, res) {
  let reservation = req.body;
  console.log(reservation);
  try {
    printer.printerReservationDress(reservation);
  } catch (err){
    res.status(500).send({msg: 'Error al crear pdf', err});
  }
  res.status(200).send({msg: 'PDF creado correctamente'});
}

function getPdfReservation(req, res){
  var pdfFile = req.params.pdfFile;
  var pathFile = `./files/pdf/${pdfFile}.pdf`;
  fs.exists(pathFile , function(exists){
    if(exists){
      res.contentType("application/pdf")
        .sendFile(path.resolve(pathFile));

    }else{
      res.status(500).send({msg: 'El archivo no fue encontrado'});
    }
  });
}

module.exports = {
  saveReservation,
  getAllReservation,
  getFilters,
  updateReservation,
  printerReservation,
  getPdfReservation
}