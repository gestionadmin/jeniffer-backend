'use strict';
let express = require('express');
let bodyParser = require('body-parser');

let app = express();

var clientRoute = require('./routes/client');
var resrvationRoute = require('./routes/reservation');
let ubicationRoute = require('./routes/ubication');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT ,DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT ,DELETE');

  next();
});
app.use('/api/client', clientRoute);
app.use('/api/reservation', resrvationRoute);
app.use('/api', ubicationRoute);

module.exports = app;
