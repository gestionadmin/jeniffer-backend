'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let CounterSchema = Schema({
  _id : String,
  seq : Number
});

module.exports = mongoose.model('Counters', CounterSchema);