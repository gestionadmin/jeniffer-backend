'use strict'

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ClientSchema = Schema({
    name : String,
    lastName : String,
    document : Number,
    phone : Number,
    line : String,
    mail : String,
    wpp : Boolean,
    street : String,
    number : Number,
    district : String,
    locality : String,
    state : String
});

module.exports = mongoose.model('Client', ClientSchema );