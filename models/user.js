'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = Schema({
    name: {
        type: String,
        required: [true, 'El campo nombre es requerido']
    },
    surname: {
        type: String,
        required: [true, 'El campo nombre es requerido']
    },
    rol: {},
    username: {},
    password: {},
    status: {}
});

module.exports = mongoose.model('User', UserSchema );