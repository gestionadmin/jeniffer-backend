'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let DepartamentShema = Schema({
  name: String
});

let LocalitySchema = Schema({
  name : String,
  departament : String
});

module.exports = mongoose.model('Departament', DepartamentShema);