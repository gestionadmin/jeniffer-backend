'use strict'

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ReservationSchema = Schema({
  numReservation : Number,
  fhReservation : String,
  fhPresentation : String,
  typeEvent: String,
  client : { type: Schema.ObjectId, ref : 'Client'},
  espalda: Number,
  hombro: Number,
  talleEspalda:Number,
  talleDelantero:Number,
  alturaBusto:Number,
  contornoBusto:Number,
  costado:Number,
  sisa:Number,
  sisa_t:Number,
  largoManga:Number,
  anchoManga:Number,
  punio:Number,
  largoFalda:Number,
  contornoCintura:Number,
  contornoCadera:Number,
  total : Number,
  description : String,
  bordado : String,
  senia : Array,
  image : String,
  state : String

});

module.exports = mongoose.model('Reservation', ReservationSchema);