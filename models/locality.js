'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let LocalitySchema = Schema({
  name : String,
  departament : String
});

module.exports = mongoose.model('Locality', LocalitySchema);